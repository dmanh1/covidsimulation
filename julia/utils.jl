function generatedata(N::Int, ρ::Float64, a::Float64, r::Float64, m::Float64,
    I₀::Float64, K::Int, tolerance::Float64=1E-07, itermax::Int=20000)
    # Generate data of stock of infected people
    # Define the transition function
    f(x::Float64)::Float64 = (1 + a*ρ*(1-r-m)*N/K)*x - a*ρ*(1-r-m)*(1+r+m)*x*x

    # Set up the loop
    pdf = zeros(itermax)
    cdf = zeros(itermax)
    act = zeros(itermax)
    pdf[1] = 0
    cdf[1] = I₀
    act[1] = I₀*(1-r-m)
    iter = 1
    x_old = I₀
    x_half = 0.5 * N / (1 + r + m)
    for i in 2:itermax
        x_new = K * f(x_old/K)
        # When it really converges
        if (x_new - x_old < tolerance) & (x_new > x_half)
            iter = i-1
            break
        else
            pdf[i] = x_new - x_old
            cdf[i] = x_new
            act[i] = (1-r-m)*act[i-1] + (1-r-m)*(x_new - x_old)
            iter = i
            x_old = x_new
        end
    end
    return iter, pdf[1:iter], cdf[1:iter], (1-r-m)*cdf[1:iter], act[1:iter]
end


function generatedata(N::Int, ρ::Float64, a::Float64, r::Float64, m::Float64,
    I₀::Float64, K::Int, start::Int, tolerance::Float64=1E-07, itermax::Int=20000)
    # Generate data of stock of infected people
    # Define the transition function

    iter, pdf, cdf, net, act = generatedata(N, ρ, a, r, m, I₀, 1,
                                                            tolerance, itermax)
    iter_1, pdf_1, cdf_1, net_1, act_1 = generatedata(N, ρ, a, r, m, cdf[start],
                                                    K, tolerance, itermax)

    iter_new = start + iter_1 - 1
    # Create pdf_new
    pdf_new = zeros(iter_new)
    pdf_new[1:start] = pdf[1:start]
    pdf_new[(start+1):iter_new] = pdf_1[2:iter_1]

    # Create cdf_new
    cdf_new = zeros(iter_new)
    cdf_new[1:(start-1)] = cdf[1:(start-1)]
    cdf_new[start:iter_new] = cdf_1[1:iter_1]

    # Create net_new
    net_new = zeros(iter_new)
    net_new[1:(start-1)] = net[1:(start-1)]
    net_new[start:iter_new] = net_1[1:iter_1]

    # Create act_new
    act_new = zeros(iter_new)
    act_new[1:(start-1)] = act[1:(start-1)]
    act_new[start:iter_new] = act_1[1:iter_1]

    return iter_new, pdf_new, cdf_new, net_new, act_new
end

function generatedata(N::Int, H::Int, γ::Float64, ρ::Float64, a::Float64,
                    r₀::Float64, m::Float64, I₀::Float64, K::Int,
                    tolerance::Float64=1E-07, itermax::Int=20000)
    # Generate data of stock of infected people
    # Define the transition function
    function f(x::Float64)
        r = r₀
        if x >= H
            r = r₀*exp(γ*(H-x)/N)
        end
        return r, (1 + a*ρ*(1-r-m)*N/K)*x - a*ρ*(1-r-m)*(1+r+m)*x*x
    end

    # Set up the loop
    pdf = zeros(itermax)
    cdf = zeros(itermax)
    net = zeros(itermax)
    act = zeros(itermax)
    pdf[1] = 0
    cdf[1] = I₀
    net[1] = (1-r₀-m)*I₀
    act[1] = (1-r₀-m)*I₀
    iter = 1
    x_old = I₀
    x_half = 0.5 * N / (1 + r₀ + m)
    for i in 2:itermax
        r, x_new = f(x_old/K)
        x_new = K * x_new
        # When it really converges
        if (x_new - x_old < tolerance) & (x_new > x_half)
            iter = i-1
            break
        else
            pdf[i] = x_new - x_old
            cdf[i] = x_new
            net[i] = (1-r-m) * x_new
            act[i] = (1-r-m)*act[i-1] + (1-r-m)*(x_new - x_old)
            iter = i
            x_old = x_new
        end
    end
    return iter, pdf[1:iter], cdf[1:iter], net[1:iter], act[1:iter]
end
