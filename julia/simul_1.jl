using Plots
include("utils.jl")

# Declare parameters to simulations
N = 10000
H = 3
γ = 3.0
I₀ = 1.0
ρ = 2.2
a = 0.0001
r = 0.85
m = 0.1
tolerance = 1E-7
itermax = 20000

#-------------------- 1: Compare I*(t) versus Ki*(t) -------------------------
n_1, pdf_1, cdf_1, net_1 = generatedata(N, ρ, a, r, m, I₀, 1)
n_2_40, pdf_2_40, cdf_2_40, net_2_40 = generatedata(N, ρ, a, r, m, I₀, 2, 40)
n_10_50, pdf_10_50, cdf_10_50, net_10_50 = generatedata(N, ρ, a, r, m, I₀, 2, 50)

plot(pdf_1/N*100)
plot!(pdf_10_50/N*100)
plot!(pdf_2_40/N*100)


plot(cdf_1/N*100)
plot!(cdf_10_60/N*100)
plot!(cdf_10_50/N*100)


n_2, pdf_2, cdf_2, net_2 = generatedata(N, ρ, a, r, m, I₀, 2)
n_10, pdf_10, cdf_10, net_10 = generatedata(N, ρ, a, r, m, I₀, 10)
n_5, pdf_5, cdf_5, net_5 = generatedata(N, ρ, a, r, m, I₀, 5)

plot(pdf_1)
plot!(pdf_2)
plot!(pdf_5)
plot!(pdf_10)


# Version 2
n_1, pdf_1, cdf_1, net_1 = generatedata(N, H, γ, ρ, a, r, m, I₀, 1)
n_2, pdf_2, cdf_2, net_2 = generatedata(N, H, γ, ρ, a, r, m, I₀, 2)
n_3, pdf_3, cdf_3, net_3 = generatedata(N, H, γ, ρ, a, r, m, I₀, 3)
n_10, pdf_10, cdf_10, net_10 = generatedata(N, H, γ, ρ, a, r, m, I₀, 10)
n_50, pdf_50, cdf_50, net_50 = generatedata(N, H, γ, ρ, a, r, m, I₀, 50)
n_80, pdf_80, cdf_80, net_80 = generatedata(N, H, γ, ρ, a, r, m, I₀, 80)

plot(pdf_1)
plot!(pdf_2)
plot!(pdf_3)
plot!(pdf_10)
plot!(pdf_50)


plot(cdf_1/N*100)
plot!(cdf_2/N*100)
plot!(cdf_3/N*100)
plot!(cdf_10/N*100)
plot!(cdf_50/N*100)
plot!(cdf_80/N*100)


plot(pdf_10)
plot!(pdf_50)


plot(cdf_10/N*100)
plot!(cdf_50/N*100)
