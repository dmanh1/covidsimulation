using Plots

tolerance=1E-8
itermax=20000

function generatedata(N, ρ, a, r, m, I₀, K)
    # Generate data of stock of infected people
    # Define the transition function
    f(x) = (1 + a*ρ*(1-r-m)*N/K)*x - a*ρ*(1-r-m)*(1+r+m)*x*x

    # Set up the loop
    pdf = zeros(itermax)
    cdf = zeros(itermax)
    pdf[1] = 0
    cdf[1] = I₀
    iter = 1
    x_old = I₀
    for i in 2:itermax
        x_new = K * f(x_old/K)
        if x_new - x_old >= tolerance
            pdf[i] = x_new - x_old
            cdf[i] = x_new
        else
            iter = i-1
            break
        end
        x_old = x_new
    end
    return (iter, pdf[1:iter], cdf[1:iter], (1-r-m)*cdf[1:iter])
end


function generatedata_v2(N, H, ρ, a, r₀, m, I₀, K)
    # Generate data of stock of infected people
    # Define the transition function
    function f(x)
        if x < H
            r = r₀
        else
            r = r₀ * exp(3*(H-x)/N)
        end
        return (1 + a*ρ*(1-r-m)*N/K)*x - a*ρ*(1-r-m)*(1+r+m)*x*x
    end

    # Set up the loop
    pdf = zeros(itermax)
    cdf = zeros(itermax)
    pdf[1] = 0
    cdf[1] = I₀
    iter = 1
    x_old = I₀
    for i in 2:itermax
        x_new = K * f(x_old/K)
        if x_new - x_old >= tolerance
            pdf[i] = x_new - x_old
            cdf[i] = x_new
        else
            iter = i-1
            break
        end
        x_old = x_new
    end
    return (iter, pdf[1:iter], cdf[1:iter], (1-r-m)*cdf[1:iter])
end

function generatedata(N, ρ, a, r, m, I₀, K, start)
    # Generate data of stock of infected people
    # Define the transition function

    iter, pdf, cdf, net = generatedata(N, ρ, a, r, m, I₀, 1)
    iter_1, pdf_1, cdf_1, net_1 = generatedata(N, ρ, a, r, m, cdf[start], K)

    iter_new = start + iter_1 - 1
    # Create pdf_new

    pdf_new = zeros(iter_new)
    pdf_new[1:start] = pdf[1:start]
    pdf_new[(start+1):iter_new] = pdf_1[2:iter_1]

    # Create cdf_new
    cdf_new = zeros(iter_new)
    cdf_new[1:(start-1)] = cdf[1:(start-1)]
    cdf_new[start:iter_new] = cdf_1[1:iter_1]

    # Create net_new
    net_new = zeros(iter_new)
    net_new[1:(start-1)] = net[1:(start-1)]
    net_new[start:iter_new] = net_1[1:iter_1]

    return (iter_new, pdf_new, cdf_new, net_new)
end

# Declare parameters to simulations
N = 10000
H = 3
I₀ = 1
ρ = 2.2
a = 0.0001
r = 0.85
m = 0.1


n_1, pdf_1, cdf_1, net_1 = generatedata(N, ρ, a, r, m, I₀, 1)
n_2_40, pdf_2_40, cdf_2_40, net_2_40 = generatedata(N, ρ, a, r, m, I₀, 2, 40)
n_10_50, pdf_10_50, cdf_10_50, net_10_50 = generatedata(N, ρ, a, r, m, I₀, 2, 50)
plot(pdf_1/N*100)
plot!(pdf_10_50/N*100)
plot!(pdf_10_60/N*100)
plot!(pdf_2_40/N*100)


plot(cdf_1/N*100)
plot!(cdf_10_60/N*100)
plot!(cdf_10_50/N*100)


n_2, pdf_2, cdf_2, net_2 = generatedata(N, ρ, a, r, m, I₀, 2)
n_10, pdf_10, cdf_10, net_10 = generatedata(N, ρ, a, r, m, I₀, 10)
n_5, pdf_5, cdf_5, net_5 = generatedata(N, ρ, a, r, m, I₀, 5)

plot(pdf_1)
plot!(pdf_2)
plot!(pdf_5)
plot!(pdf_10)


# Version 2
n_1, pdf_1, cdf_1, net_1 = generatedata_v2(N, H, ρ, a, r, m, I₀, 1)
n_2, pdf_2, cdf_2, net_2 = generatedata_v2(N, H, ρ, a, r, m, I₀, 2)
n_3, pdf_3, cdf_3, net_3 = generatedata_v2(N, H, ρ, a, r, m, I₀, 3)
n_10, pdf_10, cdf_10, net_10 = generatedata_v2(N, H, ρ, a, r, m, I₀, 10)
n_50, pdf_50, cdf_50, net_50 = generatedata_v2(N, H, ρ, a, r, m, I₀, 50)
n_80, pdf_80, cdf_80, net_80 = generatedata_v2(N, H, ρ, a, r, m, I₀, 80)

plot(pdf_1)
plot!(pdf_2)
plot!(pdf_3)
plot!(pdf_10)
plot!(pdf_50)


plot(cdf_1/N*100)
plot!(cdf_2/N*100)
plot!(cdf_3/N*100)
plot!(cdf_10/N*100)
plot!(cdf_50/N*100)
plot!(cdf_80/N*100)


plot(pdf_10)
plot!(pdf_50)


plot(cdf_10/N*100)
plot!(cdf_50/N*100)


## Professor Willam Dabb
